package ru.tsc.karbainova.tm.endpoint;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.karbainova.tm.marker.SoapCategory;

public class UserEndpointTest {
    @NonNull
    private static final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    @NonNull
    private static final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NonNull
    private static final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
    @NonNull
    private static final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @Nullable
    private static Session session;
    @Nullable User user;
    private static String userLogin = "admin";

    @BeforeClass
    public static void beforeClass() {
        session = sessionEndpoint.openSession(userLogin, userLogin);
    }

    @Test
    @Category(SoapCategory.class)
    public void findByLogin() {
        Assert.assertNotNull(adminUserEndpoint.findByLoginUser(session, "admin"));
    }

    @Test
    @Category(SoapCategory.class)
    public void findAll() {
        Assert.assertTrue(adminUserEndpoint.findAllUser(session).size() >= 1);
    }

}
